import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("./components/home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "coins",
    loadChildren: () =>
      import("./components/coins/coins.module").then((m) => m.CoinsModule),
  },
  {
    path: "currencies",
    loadChildren: () =>
      import("./components/currencies/currencies.module").then(
        (m) => m.CurrenciesModule
      ),
  },
  {
    path: "operators",
    loadChildren: () =>
      import("./components/coin-operators/coin-operators.module").then(
        (m) => m.CoinOperatorsModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
