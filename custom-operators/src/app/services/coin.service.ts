import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject, BehaviorSubject } from "rxjs";
import { Coin, CoinStatisticsTab } from "../components/models/coin";
import { CoinInformation } from "../components/models/coin-information";

@Injectable({
  providedIn: "root",
})
export class CoinService {
  private coinUrl = "api/coins";
  private coinInformationUrl = "api/coin_information";
  public coin = new Subject<Coin>();
  public infoTab = new Subject<CoinStatisticsTab>();

  constructor(private http: HttpClient) {}

  shareCoin(coin: Coin) {
    this.coin.next(coin);
  }

  shareInfoTab(infoTab: CoinStatisticsTab) {
    this.infoTab.next(infoTab);
  }

  addCoin(coin): Observable<any> {
    return this.http.post(this.coinUrl, coin);
  }

  getCoins(): Observable<Coin[]> {
    return this.http.get<Coin[]>(this.coinUrl);
  }

  getCoinById(id: number): Observable<Coin> {
    return this.http.get<Coin>(`${this.coinUrl}/${id}`);
  }

  editCoinById(payload: Coin): Observable<Coin> {
    console.log(payload);
    return this.http.put<Coin>(`${this.coinUrl}/${payload.id}`, payload);
  }

  deleteCoinById(id: number): Observable<Coin> {
    return this.http.delete<Coin>(`${this.coinUrl}/${id}`);
  }

  // Coin Information
  getCoinInformationById(id: number) {
    return this.http.get<CoinInformation>(`${this.coinInformationUrl}/${id}`);
  }
}
