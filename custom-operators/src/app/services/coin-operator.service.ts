import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { CoinOperator } from "../components/models/operator";

@Injectable({
  providedIn: "root",
})
export class CoinOperatorService {
  private coinOperatorUrl = "api/operators";

  constructor(private http: HttpClient) {}

  getCoinOperators(): Observable<CoinOperator[]> {
    return this.http.get<CoinOperator[]>(this.coinOperatorUrl);
  }

  addCoinOperator(coinOperator): Observable<any> {
    return this.http.post(this.coinOperatorUrl, coinOperator);
  }

  getCoinOperatorById(id: number): Observable<CoinOperator> {
    return this.http.get<CoinOperator>(`${this.coinOperatorUrl}/${id}`);
  }

  editCoinOperatorById(payload: CoinOperator): Observable<CoinOperator> {
    return this.http.put<CoinOperator>(
      `${this.coinOperatorUrl}/${payload.id}`,
      payload
    );
  }

  deleteCoinOperatorById(id: number): Observable<CoinOperator> {
    return this.http.delete<CoinOperator>(`${this.coinOperatorUrl}/${id}`);
  }
}
