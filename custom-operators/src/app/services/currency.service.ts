import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Currency } from "../components/models/currency";

@Injectable({
  providedIn: "root",
})
export class CurrencyService {
  private currencyUrl = "api/currencies";

  constructor(private http: HttpClient) {}

  getCurrency(): Observable<Currency[]> {
    return this.http.get<Currency[]>(this.currencyUrl);
  }

  addCurrency(currency): Observable<any> {
    return this.http.post(this.currencyUrl, currency);
  }

  getCurrencyById(id: number): Observable<Currency> {
    return this.http.get<Currency>(`${this.currencyUrl}/${id}`);
  }

  editCurrencyById(payload: Currency): Observable<Currency> {
    return this.http.put<Currency>(
      `${this.currencyUrl}/${payload.id}`,
      payload
    );
  }

  deleteCurrencyById(id: number): Observable<Currency> {
    return this.http.delete<Currency>(`${this.currencyUrl}/${id}`);
  }
}
