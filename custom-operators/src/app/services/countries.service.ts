import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Country } from "../components/models/country";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CountriesService {
  private countryUrl = "api/countries";

  constructor(private http: HttpClient) {}

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(this.countryUrl);
  }
}
