import { Injectable } from "@angular/core";
import { InMemoryDbService } from "angular-in-memory-web-api";

import { COINS } from "./mock-data/data-coin";
import { CURRENCIES } from "./mock-data/data-currency";
import { OPERATORS } from "./mock-data/data-operators";

import { Coin } from "src/app/components/models/coin";
import { Currency } from "src/app/components/models/currency";
import { CoinOperator } from "src/app/components/models/operator";
import { Country } from "src/app/components/models/country";
import { COUNTRIES } from "./mock-data/data.country";
import { CoinInformation } from "src/app/components/models/coin-information";
import { COIN_INFORMATON } from "./mock-data/data-coin-information";

@Injectable({
  providedIn: "root",
})
export class InMemoryCoinService implements InMemoryCoinService {
  createDb() {
    const coins: Coin[] = COINS;
    const currencies: Currency[] = CURRENCIES;
    const operators: CoinOperator[] = OPERATORS;
    const countries: Country[] = COUNTRIES;
    const coin_information: CoinInformation[] = COIN_INFORMATON;
    return { coins, currencies, operators, countries, coin_information };
  }
}
