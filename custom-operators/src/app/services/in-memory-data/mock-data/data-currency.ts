import { Currency } from "src/app/components/models/currency";

export const CURRENCIES: Currency[] = [
  {
    id: 1,
    shortName: "USD",
    name: "American Dollar",
    emisor: "US Federal Reserve",
  },
  {
    id: 2,
    shortName: "CA",
    name: "Canadian Dollar",
    emisor: "Royal Bank of Canada",
  },
  { id: 3, shortName: "EU", name: "Euro", emisor: "Central European Bank" },
  {
    id: 4,
    shortName: "ARS",
    name: "Argentinian Peso",
    emisor: "Banco Central de la Republica Argentina",
  },
  {
    id: 5,
    shortName: "RE",
    name: "Brazilian Reai",
    emisor: "Banco Central do Brasil",
  },
  {
    id: 6,
    shortName: "YU",
    name: "Chinese Yuan",
    emisor: "People's Bank of China",
  },
  {
    id: 7,
    shortName: "YE",
    name: "Japanense Yen",
    emisor: "Imperial Japanese Bank",
  },
];
