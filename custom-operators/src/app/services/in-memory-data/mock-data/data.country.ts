import { Country } from "../../../components/models/country";

export const COUNTRIES: Country[] = [
  { id: 1, abbreviation: "USA", name: "United States" },
  { id: 2, abbreviation: "CAN", name: "Canada" },
  { id: 3, abbreviation: "ARG", name: "Argentina" },
  { id: 4, abbreviation: "MEX", name: "Mexico" },
  { id: 5, abbreviation: "BRA", name: "Brazil" },
  { id: 6, abbreviation: "SPA", name: "Spain" },
  { id: 7, abbreviation: "GER", name: "Germany" },
];
