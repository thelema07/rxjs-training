import { CoinOperator } from "../../../components/models/operator";

export const OPERATORS: CoinOperator[] = [
  {
    id: 1,
    name: "Bernard",
    lastName: "Marx",
    country: "United Kingdom",
    permission: "System Admin",
    clearance: "Alpha",
  },
  {
    id: 2,
    name: "Mustapha",
    lastName: "Mond",
    country: "United States",
    permission: "System Admin",
    clearance: "Alpha",
  },
  {
    id: 3,
    name: "Lenina",
    lastName: "Crowne",
    country: "Virgin Islands",
    permission: "Level A",
    clearance: "Beta",
  },
  {
    id: 4,
    name: "John",
    lastName: "The Savage",
    country: "Iraq",
    permission: "Level B",
    clearance: "Beta",
  },
  {
    id: 5,
    name: "Philippe",
    lastName: "Descartes",
    country: "Switzerland",
    permission: "Level C",
    clearance: "Epsilon",
  },
];
