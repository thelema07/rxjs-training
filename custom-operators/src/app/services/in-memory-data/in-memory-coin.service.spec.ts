import { TestBed } from '@angular/core/testing';

import { InMemoryCoinService } from './in-memory-coin.service';

describe('InMemoryCoinService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InMemoryCoinService = TestBed.get(InMemoryCoinService);
    expect(service).toBeTruthy();
  });
});
