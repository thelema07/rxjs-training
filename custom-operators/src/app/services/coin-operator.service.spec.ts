import { TestBed } from '@angular/core/testing';

import { CoinOperatorService } from './coin-operator.service';

describe('CoinOperatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoinOperatorService = TestBed.get(CoinOperatorService);
    expect(service).toBeTruthy();
  });
});
