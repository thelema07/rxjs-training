import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinOperatorsComponent } from './coin-operators.component';

describe('CoinOperatorsComponent', () => {
  let component: CoinOperatorsComponent;
  let fixture: ComponentFixture<CoinOperatorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinOperatorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinOperatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
