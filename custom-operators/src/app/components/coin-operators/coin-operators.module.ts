import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CoinOperatorsRoutingModule } from "./coin-operators-routing.module";
import { CoinOperatorsComponent } from "./coin-operators.component";
import { CoinOperatorsListComponent } from "./coin-operators-list/coin-operators-list.component";
import { AddCoinOperatorComponent } from "./add-coin-operator/add-coin-operator.component";
import { PreventUnsavedAddCoinOperator } from "src/app/guards/prevent-unsaved-add-coin-operator.guard";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";

@NgModule({
  declarations: [
    CoinOperatorsComponent,
    CoinOperatorsListComponent,
    AddCoinOperatorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoinOperatorsRoutingModule,
    BsDropdownModule.forRoot(),
  ],
  providers: [PreventUnsavedAddCoinOperator],
})
export class CoinOperatorsModule {}
