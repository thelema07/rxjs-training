import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CoinOperatorsComponent } from "./coin-operators.component";
import { AddCoinOperatorComponent } from "./add-coin-operator/add-coin-operator.component";
import { PreventUnsavedAddCoinOperator } from "src/app/guards/prevent-unsaved-add-coin-operator.guard";

const routes: Routes = [
  { path: "", component: CoinOperatorsComponent },
  {
    path: "add",
    component: AddCoinOperatorComponent,
    canDeactivate: [PreventUnsavedAddCoinOperator],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoinOperatorsRoutingModule {}
