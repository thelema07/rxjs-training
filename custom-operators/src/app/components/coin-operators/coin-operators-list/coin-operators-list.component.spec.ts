import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinOperatorsListComponent } from './coin-operators-list.component';

describe('CoinOperatorsListComponent', () => {
  let component: CoinOperatorsListComponent;
  let fixture: ComponentFixture<CoinOperatorsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinOperatorsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinOperatorsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
