import { Component, OnInit, Input } from "@angular/core";
import { CoinOperator } from "../../models/operator";

@Component({
  selector: "app-coin-operators-list",
  templateUrl: "./coin-operators-list.component.html",
  styleUrls: ["./coin-operators-list.component.scss"],
})
export class CoinOperatorsListComponent implements OnInit {
  @Input() coinOperators: CoinOperator[];

  constructor() {}

  ngOnInit() {}
}
