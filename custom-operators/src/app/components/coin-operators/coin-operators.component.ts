import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { CoinOperator } from "../models/operator";
import { CoinOperatorService } from "src/app/services/coin-operator.service";
import { Subscriber } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "app-coin-operators",
  templateUrl: "./coin-operators.component.html",
  styleUrls: ["./coin-operators.component.scss"],
})
export class CoinOperatorsComponent implements OnInit {
  subscriptions = new Subscriber();
  coinOperators: CoinOperator[];
  isLoading: boolean;

  constructor(
    private coinOperatorService: CoinOperatorService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getCurrencies();
  }

  getCurrencies() {
    this.isLoading = true;
    const coinOperatorsSubscription = this.coinOperatorService
      .getCoinOperators()
      .subscribe((response: CoinOperator[]) => {
        this.coinOperators = response;
        this.isLoading = false;
      });

    this.subscriptions.add(coinOperatorsSubscription);
  }

  addCoinOperator() {
    this.router.navigate(["operators/add"]);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
