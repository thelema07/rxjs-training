import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCoinOperatorComponent } from './add-coin-operator.component';

describe('AddCoinOperatorComponent', () => {
  let component: AddCoinOperatorComponent;
  let fixture: ComponentFixture<AddCoinOperatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCoinOperatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCoinOperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
