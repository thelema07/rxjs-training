import { Component, OnInit, HostListener } from "@angular/core";
import { CoinOperatorService } from "src/app/services/coin-operator.service";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { Subscription } from "rxjs";
import { CountriesService } from "src/app/services/countries.service";
import { Country } from "../../models/country";

@Component({
  selector: "app-add-coin-operator",
  templateUrl: "./add-coin-operator.component.html",
  styleUrls: ["./add-coin-operator.component.scss"],
})
export class AddCoinOperatorComponent implements OnInit {
  @HostListener("window:beforeunload", ["$event"])
  unloadNotification($event: any) {
    if (this.coinOperatorsForm.dirty) {
      $event.returnValue = true;
    }
  }

  private subscriptions: Subscription = new Subscription();
  coinOperatorsForm: FormGroup;
  countries: Country[];

  constructor(
    private form: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private coinOperatorsService: CoinOperatorService,
    private countriesService: CountriesService
  ) {}

  ngOnInit() {
    this.initForm();
    this.getCountryData();
  }

  initForm() {
    this.coinOperatorsForm = this.form.group({
      name: new FormControl("", [Validators.required, Validators.minLength(3)]),
      lastName: new FormControl("", [
        Validators.required,
        Validators.minLength(3),
      ]),
      country: new FormControl("Argentina", [
        Validators.required,
        Validators.minLength(3),
      ]),
      permission: new FormControl("", [
        Validators.required,
        Validators.minLength(3),
      ]),
      clearance: new FormControl("", [
        Validators.required,
        Validators.minLength(3),
      ]),
    });
  }

  addCoinOperator() {
    console.log(this.coinOperatorsForm.value);
    // Todo Spinner
    if (this.coinOperatorsForm.valid) {
      const addCurrencySubscription = this.coinOperatorsService
        .addCoinOperator(this.coinOperatorsForm.value)
        .subscribe((response) => {
          this.coinOperatorsForm.reset();
          // implement modal reusable component
          this.router.navigate(["operators"]);
        });

      this.subscriptions.add(addCurrencySubscription);
    } else {
      // Must display alert modal
      alert("Validation Error");
    }
  }

  onchangeCountry(country: string) {
    this.coinOperatorsForm.controls["country"].patchValue(country);
  }

  getCountryData() {
    this.countriesService.getCountries().subscribe((response: Country[]) => {
      this.countries = response;
    });
  }

  goToCoins() {
    this.router.navigate(["operators"]);
  }

  validateCurrencyForm() {
    return this.coinOperatorsForm.invalid;
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
