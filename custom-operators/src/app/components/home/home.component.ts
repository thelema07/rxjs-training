import { Component, OnInit, OnDestroy } from "@angular/core";
import { CoinService } from "src/app/services/coin.service";
import { Coin } from "../models/coin";
import { CurrencyService } from "src/app/services/currency.service";
import { Subscriber } from "rxjs";
import { Currency } from "../models/currency";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
