import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { CurrencyService } from "src/app/services/currency.service";
import { Currency } from "../../models/currency";
import { Observable } from "rxjs";

@Component({
  selector: "app-currency-list",
  templateUrl: "./currency-list.component.html",
  styleUrls: ["./currency-list.component.scss"],
})
export class CurrencyListComponent implements OnInit {
  @Input() currencies: Observable<Currency[]>;

  constructor(
    private currencyService: CurrencyService,
    private router: Router
  ) {}

  ngOnInit() {}

  editCoin() {
    // this.router.navigate(["edit", coin.id]);
  }
}
