import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CurrenciesRoutingModule } from "./currencies-routing.module";
import { CurrenciesComponent } from "./currencies.component";
import { CurrencyListComponent } from "./currency-list/currency-list.component";
import { AddCurrencyComponent } from "./add-currency/add-currency.component";
import { PreventUnsavedAddCurrency } from "src/app/guards/prevent-unsaved-add-currency.guard";

@NgModule({
  declarations: [
    CurrenciesComponent,
    CurrencyListComponent,
    AddCurrencyComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CurrenciesRoutingModule,
  ],
  providers: [PreventUnsavedAddCurrency],
})
export class CurrenciesModule {}
