import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CurrenciesComponent } from "./currencies.component";
import { AddCurrencyComponent } from "./add-currency/add-currency.component";
import { PreventUnsavedAddCurrency } from "src/app/guards/prevent-unsaved-add-currency.guard";

const routes: Routes = [
  { path: "", component: CurrenciesComponent },
  {
    path: "add",
    component: AddCurrencyComponent,
    canDeactivate: [PreventUnsavedAddCurrency],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrenciesRoutingModule {}
