import { Component, OnInit, HostListener } from "@angular/core";
import { Subscription } from "rxjs";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { CurrencyService } from "src/app/services/currency.service";

@Component({
  selector: "app-add-currency",
  templateUrl: "./add-currency.component.html",
  styleUrls: ["./add-currency.component.scss"],
})
export class AddCurrencyComponent implements OnInit {
  @HostListener("window:beforeunload", ["$event"])
  unloadNotification($event: any) {
    if (this.currencyForm.dirty) {
      $event.returnValue = true;
    }
  }

  private subscriptions: Subscription = new Subscription();
  currencyForm: FormGroup;

  constructor(
    private form: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private currencyService: CurrencyService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.currencyForm = this.form.group({
      shortName: new FormControl("", [
        Validators.required,
        Validators.minLength(3),
      ]),
      name: new FormControl("", [Validators.required, Validators.minLength(3)]),
      emisor: new FormControl("", [
        Validators.required,
        Validators.minLength(3),
      ]),
    });
  }

  addCoin() {
    console.log(this.currencyForm.value);
    // Todo Spinner
    if (this.currencyForm.valid) {
      const addCurrencySubscription = this.currencyService
        .addCurrency(this.currencyForm.value)
        .subscribe((response) => {
          this.currencyForm.reset();
          console.log(response);
          // implement modal reusable component
          this.router.navigate(["currencies"]);
        });

      this.subscriptions.add(addCurrencySubscription);
    } else {
      // Must display alert modal
      alert("Validation Error");
    }
  }

  onSubmit() {}

  goToCurrencies() {
    this.router.navigate(["currencies"]);
  }

  validateCurrencyForm() {
    return this.currencyForm.invalid;
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
