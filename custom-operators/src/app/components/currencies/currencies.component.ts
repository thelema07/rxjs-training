import { Component, OnInit } from "@angular/core";
import { Subscriber, Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { Coin } from "../models/coin";
import { Currency } from "../models/currency";
import { CoinService } from "src/app/services/coin.service";
import { CurrencyService } from "src/app/services/currency.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-currencies",
  templateUrl: "./currencies.component.html",
  styleUrls: ["./currencies.component.scss"],
})
export class CurrenciesComponent implements OnInit {
  subscriptions = new Subscriber();
  currencies: Currency[];
  currencies$: Observable<Currency[]>;
  isLoading: boolean;

  constructor(
    private currencyService: CurrencyService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getCurrencies();
  }

  getCurrencies() {
    this.isLoading = true;
    this.currencies$ = this.currencyService.getCurrency();
    this.isLoading = false;
  }

  addCurrency() {
    this.router.navigate(["currencies/add"]);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
