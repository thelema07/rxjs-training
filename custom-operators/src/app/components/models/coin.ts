export interface Coin {
  id: number;
  shortName: string;
  name: string;
  currency?: string;
  price?: number;
  rate?: number;
}

export interface CoinStatisticsTab {
  id: number;
  active: boolean;
}
