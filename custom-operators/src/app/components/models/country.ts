export interface Country {
  id: number;
  abbreviation: string;
  name: string;
}
