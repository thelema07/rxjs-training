export interface Currency {
  id: number;
  shortName: string;
  name: string;
  emisor?: string;
}
