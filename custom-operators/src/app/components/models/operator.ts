export interface CoinOperator {
  id: number;
  name: string;
  lastName: string;
  country: string;
  permission: string;
  clearance: string;
}
