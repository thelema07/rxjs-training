export interface CoinInformation {
  id: number;
  name: string;
  description: string;
  network: string;
  providers: string[];
}
