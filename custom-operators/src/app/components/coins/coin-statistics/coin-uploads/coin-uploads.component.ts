import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-coin-uploads',
  templateUrl: './coin-uploads.component.html',
  styleUrls: ['./coin-uploads.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoinUploadsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
