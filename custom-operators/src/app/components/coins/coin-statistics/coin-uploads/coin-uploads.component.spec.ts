import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinUploadsComponent } from './coin-uploads.component';

describe('CoinUploadsComponent', () => {
  let component: CoinUploadsComponent;
  let fixture: ComponentFixture<CoinUploadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinUploadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinUploadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
