import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from "@angular/core";
import { CoinService } from "src/app/services/coin.service";
import { CoinStoreService } from "src/app/services/coin-store.service";
import { Coin } from "src/app/components/models/coin";

@Component({
  selector: "app-coin-information",
  templateUrl: "./coin-information.component.html",
  styleUrls: ["./coin-information.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoinInformationComponent implements OnInit {
  @Input() coinInformationId: number;
  infoCoins: Coin[];

  constructor(
    private coinService: CoinService,
    private coinStoreService: CoinStoreService
  ) {}

  ngOnInit() {
    this.coinStoreService.coins.subscribe((res) => {
      console.log(res);
    });
  }
}
