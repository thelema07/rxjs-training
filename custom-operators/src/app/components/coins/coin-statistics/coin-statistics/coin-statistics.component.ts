import { Component, OnInit, OnDestroy } from "@angular/core";
import { CoinService } from "src/app/services/coin.service";
import { Subscriber } from "rxjs";
import { CoinStatisticsTab } from "src/app/components/models/coin";

@Component({
  selector: "app-coin-statistics",
  templateUrl: "./coin-statistics.component.html",
  styleUrls: ["./coin-statistics.component.scss"],
})
export class CoinStatisticsComponent implements OnInit, OnDestroy {
  subscriptions = new Subscriber();
  activeTab: boolean = false;
  coinStatsData = {
    coinInformation: {},
    coinGraphics: {},
  };
  coinId: number | null;

  constructor(private coinService: CoinService) {}

  ngOnInit() {
    this.subscriptions.add(
      this.coinService.infoTab.subscribe((tab: CoinStatisticsTab) => {
        this.activeTab = tab.active;
        console.log(tab);
        this.coinId = tab.id;
        // Should Use combineLatest when get all the tab fields organized
      })
    );
  }

  onClickOutSideTab() {}

  disableTab() {
    return !this.coinId ? true : false;
  }

  hideStatisticsTab() {
    this.activeTab = !this.activeTab;
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
