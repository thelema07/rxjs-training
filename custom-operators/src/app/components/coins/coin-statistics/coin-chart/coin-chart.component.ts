import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-coin-chart',
  templateUrl: './coin-chart.component.html',
  styleUrls: ['./coin-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoinChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
