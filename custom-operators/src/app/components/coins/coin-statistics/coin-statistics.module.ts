import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CoinStatisticsComponent } from "./coin-statistics/coin-statistics.component";
import { TabsModule } from "ngx-bootstrap/tabs";
import { CoinInformationComponent } from './coin-information/coin-information.component';
import { CoinChartComponent } from './coin-chart/coin-chart.component';
import { CoinUploadsComponent } from './coin-uploads/coin-uploads.component';

@NgModule({
  declarations: [CoinStatisticsComponent, CoinInformationComponent, CoinChartComponent, CoinUploadsComponent],
  imports: [CommonModule, TabsModule.forRoot()],
  exports: [CoinStatisticsComponent],
})
export class CoinStatisticsModule {}
