import { Component, OnInit, Input } from "@angular/core";
import { Coin } from "../../models/coin";
import { CoinService } from "src/app/services/coin.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-coins-list",
  templateUrl: "./coins-list.component.html",
  styleUrls: ["./coins-list.component.scss"],
})
export class CoinsListComponent implements OnInit {
  @Input() coins: Coin[];

  constructor(private coinService: CoinService, private router: Router) {}

  ngOnInit() {}

  editCoin(coin: Coin) {
    this.router.navigate(["edit", coin.id]);
  }

  shareCoin(coin: Coin) {
    this.coinService.shareCoin(coin);
    this.coinService.shareInfoTab({ id: coin.id, active: true });
  }
}
