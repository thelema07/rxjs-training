import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CoinsRoutingModule } from "./coins-routing.module";
import { CoinsComponent } from "./coins.component";
import { CoinsListComponent } from "./coins-list/coins-list.component";
import { AddCoinComponent } from "./add-coin/add-coin.component";
import { PreventUnsavedAddCoin } from "src/app/guards/prevent-unsaved-add-coin.guard";
import { CoinStatisticsModule } from './coin-statistics/coin-statistics.module';

@NgModule({
  declarations: [CoinsComponent, CoinsListComponent, AddCoinComponent],
  imports: [CommonModule, CoinsRoutingModule, FormsModule, ReactiveFormsModule, CoinStatisticsModule],
  providers: [PreventUnsavedAddCoin],
})
export class CoinsModule {}
