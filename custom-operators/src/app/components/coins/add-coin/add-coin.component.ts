import { Component, OnInit, OnDestroy, HostListener } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { CoinService } from "src/app/services/coin.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { Currency } from "../../models/currency";
import { CurrencyService } from "src/app/services/currency.service";

@Component({
  selector: "app-add-coin",
  templateUrl: "./add-coin.component.html",
  styleUrls: ["./add-coin.component.scss"],
})
export class AddCoinComponent implements OnInit, OnDestroy {
  @HostListener("window:beforeunload", ["$event"])
  unloadNotification($event: any) {
    if (this.coinForm.dirty) {
      $event.returnValue = true;
    }
  }

  private subscriptions: Subscription = new Subscription();
  defaultSelect = "American Dollar";
  coinForm: FormGroup;
  headTitle: string;
  isAdd: boolean;
  currencies: Currency[];

  constructor(
    private form: FormBuilder,
    private coinService: CoinService,
    private route: ActivatedRoute,
    private router: Router,
    private currencyService: CurrencyService
  ) {}

  get coin() {
    const route = this.route.snapshot.paramMap.get("id");
    if (route !== null) {
      this.headTitle = "Edit";
    }
    this.headTitle = "Add";
    return null;
  }

  ngOnInit() {
    if (this.coin === null) {
      this.isAdd = true;
    } else {
      this.isAdd = false;
    }
    this.initForm();
    this.getCurrenciesData();
  }

  initForm() {
    if (this.isAdd) {
      this.coinForm = this.form.group({
        shortName: new FormControl("", [
          Validators.required,
          Validators.minLength(3),
        ]),
        name: new FormControl("", [
          Validators.required,
          Validators.minLength(3),
        ]),
        currency: new FormControl("", [Validators.required]),
        price: new FormControl(0, [Validators.required]),
        rate: new FormControl(0, [Validators.required]),
      });
    } else {
      this.coinForm = this.form.group({
        shortName: new FormControl(this.coin.shortName, [
          Validators.required,
          Validators.minLength(3),
        ]),
        name: new FormControl(this.coin.name, [
          Validators.required,
          Validators.minLength(3),
        ]),
        currency: new FormControl(this.coin.currency, [Validators.required]),
        price: new FormControl(this.coin.price.toFixed(2), [
          Validators.required,
        ]),
        rate: new FormControl(this.coin.rate.toFixed(2), [Validators.required]),
      });
    }
  }

  addCoin() {
    // Todo Spinner
    console.log(this.coinForm.value);
    if (this.coinForm.valid && this.isAdd) {
      const addCoinSubscription = this.coinService
        .addCoin(this.coinForm.value)
        .subscribe((response) => {
          this.coinForm.reset();
          this.router.navigate(["coins"]);
        });

      this.subscriptions.add(addCoinSubscription);
    } else if (this.coinForm.valid && !this.isAdd) {
      const payload = {
        id: Number(this.route.snapshot.paramMap.get("id")),
        ...this.coinForm.value,
      };
      const editCoinSubscription = this.coinService
        .editCoinById(payload)
        .subscribe((response) => {
          this.coinForm.reset();
          this.goToCoins();
        });

      this.subscriptions.add(editCoinSubscription);
    }
  }

  onSubmit() {}

  getCurrenciesData() {
    this.currencyService.getCurrency().subscribe((response: Currency[]) => {
      this.currencies = response;
    });
  }

  onchangeCurrency(currency) {
    console.log(currency);
  }

  goToCoins() {
    this.router.navigate(["coins"]);
  }

  validateCoinForm() {
    return this.coinForm.invalid;
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
