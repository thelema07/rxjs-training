import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { CoinService } from "src/app/services/coin.service";
import { Subscriber, Observable } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Coin } from "../models/coin";
import { Router } from "@angular/router";
import { CoinStoreService } from "src/app/services/coin-store.service";

@Component({
  selector: "app-coins",
  templateUrl: "./coins.component.html",
  styleUrls: ["./coins.component.scss"],
})
export class CoinsComponent implements OnInit, OnDestroy {
  modalRef: BsModalRef;
  isLoading: boolean;
  subscriptions = new Subscriber();
  coins: Coin[] = [];
  coins$: Observable<Coin[]>;
  currencyFormMode: string;

  constructor(
    private coinService: CoinService,
    private coinStoreService: CoinStoreService,
    private router: Router,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.getCoins();
    this.coinStoreService.fetchCoins();
    this.coins$ = this.coinStoreService.coins;
    console.log(this.coins$);
  }

  getCoins() {
    this.isLoading = true;
    const coinSubscription = this.coinService
      .getCoins()
      .subscribe((response: Coin[]) => {
        this.coins = response;
        this.isLoading = false;
      });
    this.subscriptions.add(coinSubscription);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  addCoin() {
    this.router.navigate(["coins/add"]);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
