import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CoinsComponent } from "./coins.component";
import { AddCoinComponent } from "./add-coin/add-coin.component";
import { PreventUnsavedAddCoin } from "src/app/guards/prevent-unsaved-add-coin.guard";

const routes: Routes = [
  { path: "", component: CoinsComponent },
  {
    path: "add",
    component: AddCoinComponent,
    canDeactivate: [PreventUnsavedAddCoin],
  },
  {
    path: "edit/:id",
    component: AddCoinComponent,
    canDeactivate: [PreventUnsavedAddCoin],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoinsRoutingModule {}
