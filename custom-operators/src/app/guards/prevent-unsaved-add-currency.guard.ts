import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { AddCurrencyComponent } from "../components/currencies/add-currency/add-currency.component";

@Injectable()
export class PreventUnsavedAddCurrency
  implements CanDeactivate<AddCurrencyComponent> {
  canDeactivate(component: AddCurrencyComponent) {
    if (component.currencyForm.dirty) {
      // todo: implement modal service
      return confirm("Are you sure? your currency changes will be lost");
    }
    return true;
  }
}
