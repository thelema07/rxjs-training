import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { AddCoinComponent } from "../components/coins/add-coin/add-coin.component";

@Injectable()
export class PreventUnsavedAddCoin implements CanDeactivate<AddCoinComponent> {
  canDeactivate(component: AddCoinComponent) {
    if (component.coinForm.dirty) {
      // todo: implement modal service
      return confirm("Are you sure? your changes will be lost");
    }
    return true;
  }
}
