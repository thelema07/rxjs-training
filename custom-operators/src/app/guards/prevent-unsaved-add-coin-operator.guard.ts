import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { AddCoinOperatorComponent } from "../components/coin-operators/add-coin-operator/add-coin-operator.component";

@Injectable()
export class PreventUnsavedAddCoinOperator
  implements CanDeactivate<AddCoinOperatorComponent> {
  canDeactivate(component: AddCoinOperatorComponent) {
    if (
      component.coinOperatorsForm.dirty ||
      (component.coinOperatorsForm.dirty &&
        component.coinOperatorsForm.untouched)
    ) {
      // todo: implement modal service
      return confirm("Are you sure? your currency changes will be lost");
    }
    return true;
  }
}
