import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { HttpClientInMemoryWebApiModule } from "angular-in-memory-web-api";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// Bootstrap Modules
import { ModalModule } from "ngx-bootstrap/modal";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TabsModule } from "ngx-bootstrap/tabs";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SharedModule } from "./shared/shared.module";
import { HomeModule } from "./components/home/home.module";
import { CoinsModule } from "./components/coins/coins.module";
import { InMemoryCoinService } from "./services/in-memory-data/in-memory-coin.service";
import { CoinService } from "./services/coin.service";
import { CurrencyService } from "./services/currency.service";
import { CurrenciesModule } from "./components/currencies/currencies.module";
import { CoinOperatorsModule } from "./components/coin-operators/coin-operators.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HomeModule,
    CoinsModule,
    CurrenciesModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryCoinService, {
      dataEncapsulation: false,
    }),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    CoinOperatorsModule,
  ],
  providers: [CoinService, CurrencyService],
  bootstrap: [AppComponent],
})
export class AppModule {}
